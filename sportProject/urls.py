from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from sportApp import view as viewsProduct  

urlpatterns = [
    path('productos/', viewsProduct.product_api_view),
    path('busquedad_productos/<int:pk>', viewsProduct.product_detail_view),
]
