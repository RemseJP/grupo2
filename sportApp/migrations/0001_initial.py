# Generated by Django 3.2.8 on 2021-10-08 18:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='productos',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=30)),
                ('precio', models.IntegerField(default=0)),
                ('categoria', models.CharField(max_length=30)),
            ],
        ),
    ]
